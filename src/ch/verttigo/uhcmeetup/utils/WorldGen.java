package ch.verttigo.uhcmeetup.utils;

import net.minecraft.server.v1_8_R3.BiomeBase;
import org.bukkit.generator.ChunkGenerator;

import java.lang.reflect.Field;

public class WorldGen extends ChunkGenerator {
    public static void setup() {
        try {
            final Field f = BiomeBase.class.getDeclaredField("biomes");
            f.setAccessible(true);
            if (f.get(null) instanceof BiomeBase[]) {
                final BiomeBase[] b = (BiomeBase[]) f.get(null);
                b[BiomeBase.DEEP_OCEAN.id] = BiomeBase.PLAINS;
                b[BiomeBase.OCEAN.id] = BiomeBase.PLAINS;
                f.set(null, b);
            }
        } catch (Exception ex) {
        }
        Field f2 = null;
        try {
            f2 = BiomeBase.class.getDeclaredField("biomes");
            f2.setAccessible(true);
            if (f2.get(null) instanceof BiomeBase[]) {
                final BiomeBase[] b = (BiomeBase[]) f2.get(null);
                b[BiomeBase.DEEP_OCEAN.id] = BiomeBase.PLAINS;
                b[BiomeBase.OCEAN.id] = BiomeBase.PLAINS;
                b[BiomeBase.SWAMPLAND.id] = BiomeBase.PLAINS;
                b[BiomeBase.JUNGLE.id] = BiomeBase.PLAINS;
                b[BiomeBase.JUNGLE_EDGE.id] = BiomeBase.PLAINS;
                b[BiomeBase.JUNGLE_HILLS.id] = BiomeBase.PLAINS;
                b[BiomeBase.EXTREME_HILLS.id] = BiomeBase.PLAINS;
                b[BiomeBase.EXTREME_HILLS_PLUS.id] = BiomeBase.PLAINS;
                b[BiomeBase.MESA.id] = BiomeBase.PLAINS;
                b[BiomeBase.MESA_PLATEAU.id] = BiomeBase.PLAINS;
                b[BiomeBase.MESA_PLATEAU_F.id] = BiomeBase.PLAINS;
                b[BiomeBase.TAIGA.id] = BiomeBase.PLAINS;
                b[BiomeBase.MEGA_TAIGA.id] = BiomeBase.PLAINS;
                b[BiomeBase.TAIGA_HILLS.id] = BiomeBase.PLAINS;
                BiomeBase[] b2;
                for (int j = (b2 = BiomeBase.getBiomes()).length, i = 0; i < j; ++i) {
                    final BiomeBase lb = b2[i];
                    b[lb.id] = BiomeBase.PLAINS;
                }
                f2.set(null, b);
            }
        } catch (Exception ex2) {
        }
    }
}
