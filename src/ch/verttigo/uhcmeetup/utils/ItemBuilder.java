package ch.verttigo.uhcmeetup.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.List;

public class ItemBuilder {
    private Material material;
    private Integer amount;
    private int data;
    private String name;
    private List<String> lore;
    private boolean unbreakable;
    private HashMap<Enchantment, Integer> enchantment;
    private ItemFlag[] flags;

    public ItemBuilder() {
        this.data = 0;
        this.unbreakable = false;
    }

    public ItemBuilder setMaterial(final Material material) {
        this.material = material;
        return this;
    }

    public ItemBuilder setAmount(final Integer amount) {
        this.amount = amount;
        return this;
    }

    public ItemBuilder setData(final int data) {
        this.data = data;
        return this;
    }

    public ItemBuilder setName(final String name) {
        this.name = name;
        return this;
    }

    public ItemBuilder setLore(final List<String> lore) {
        this.lore = lore;
        return this;
    }

    public ItemBuilder setUnbreakable(final boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    public ItemBuilder setEnchantment(final HashMap<Enchantment, Integer> enchantment) {
        this.enchantment = enchantment;
        return this;
    }

    public ItemBuilder withItemFlags(final ItemFlag... flags) {
        this.flags = flags;
        return this;
    }

    public ItemStack build() {
        final ItemStack item = new ItemStack(this.material);
        final ItemMeta im = item.getItemMeta();
        if (this.amount != null) {
            item.setAmount(this.amount);
        }
        if (this.data != 0) {
            item.setDurability((short) this.data);
        }
        if (this.name != null) {
            im.setDisplayName(this.name);
        }
        if (this.lore != null) {
            im.setLore(this.lore);
        }
        if (this.flags != null) {
            im.addItemFlags(this.flags);
        }
        if (this.unbreakable) {
            im.spigot().setUnbreakable(true);
        }
        if (this.enchantment != null) {
            item.addEnchantments(this.enchantment);
        }
        item.setItemMeta(im);
        return item;
    }
}
