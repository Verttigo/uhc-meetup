package ch.verttigo.uhcmeetup.utils;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FileManager {
    private File folder;
    private String fileName;
    private File file;
    private FileConfiguration configuration;
    private Plugin plugin;

    public FileManager(final String name, final String folder) {
        this.plugin = UHCMeetUp.getInstance();
        this.fileName = name;
        this.folder = new File(UHCMeetUp.getInstance().getDataFolder().getPath() + folder);
        this.loadFile();
    }

    public static ArrayList getEveryConfig(final File folder) {
        final ArrayList<String> listFiles = new ArrayList<String>();
        if (folder.exists() && folder.isDirectory()) {
            for (final File entry : folder.listFiles()) {
                if (!listFiles.contains(entry)) {
                    listFiles.add(entry.getName());
                }
            }
        }
        return listFiles;
    }

    public File getFile() {
        return this.file;
    }

    public File getFolder() {
        return this.folder;
    }

    public FileConfiguration getConfiguration() {
        return this.configuration;
    }

    public String getFileName() {
        return this.fileName;
    }

    private void loadFile() {
        this.file = new File(this.folder, this.fileName);
        try {
            if (!this.folder.exists()) {
                this.folder.mkdir();
            }
            if (!this.file.exists()) {
                this.file.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.configuration = YamlConfiguration.loadConfiguration(this.file);
        this.save();
    }

    public void save() {
        try {
            this.configuration.save(this.file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        if (this.file.exists()) {
            this.file.delete();
        }
    }

    public void reload() {
        this.loadFile();
        this.save();
    }
}
