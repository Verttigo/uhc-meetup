package ch.verttigo.uhcmeetup.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class Utils {
    public static int getChestSize(final int size) {
        int chestSize = 0;
        if (size <= 9) {
            chestSize = 9;
        } else if (size > 9 && size <= 18) {
            chestSize = 18;
        } else if (size > 18 && size <= 27) {
            chestSize = 27;
        } else if (size > 27 && size <= 36) {
            chestSize = 36;
        } else if (size > 36 && size <= 45) {
            chestSize = 45;
        } else if (size > 45 && size <= 54) {
            chestSize = 54;
        } else {
            chestSize = 9;
        }
        return chestSize;
    }

    public static void giveLobbyTools(final Player p) {
        final ItemBuilder vote = new ItemBuilder();
        vote.setMaterial(Material.CHEST);
        vote.setName(ChatColor.translateAlternateColorCodes('§', "§aScenario Vote"));
        vote.setAmount(1);
        p.getInventory().setItem(0, vote.build());
        if (p.hasPermission("uhcmeetup.forcestart")) {
            final ItemBuilder start = new ItemBuilder();
            start.setMaterial(Material.COMMAND_MINECART);
            start.setName(ChatColor.translateAlternateColorCodes('§', "§aForceStart"));
            start.setAmount(1);
            p.getInventory().setItem(8, start.build());
        }
    }
}
