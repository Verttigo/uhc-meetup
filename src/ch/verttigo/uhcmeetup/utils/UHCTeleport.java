package ch.verttigo.uhcmeetup.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.Random;

public class UHCTeleport
{
    public static void tpRandom(final Player p) {
        final Random r = new Random();
        final int x = r.nextInt(145);
        final int y = 128;
        final int z = -r.nextInt(145);
        final World world = Bukkit.getWorld("UHC_World");
        final Location randomLoc = new Location(world, x, 128.0, z);
        p.teleport(randomLoc);
    }
}
