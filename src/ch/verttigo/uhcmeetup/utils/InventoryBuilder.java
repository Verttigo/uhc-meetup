package ch.verttigo.uhcmeetup.utils;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class InventoryBuilder {
    private String invName;
    private int invSize;
    private InventoryHolder invHolder;
    private Map<Integer, ItemStack> invItems;
    private ItemStack fillerItem;
    private boolean useFiller;

    public InventoryBuilder(final String name, final int size) {
        this.invItems = new HashMap<Integer, ItemStack>();
        this.useFiller = false;
        this.invName = name;
        this.invSize = size;
    }

    public InventoryBuilder setName(final String name) {
        this.invName = name;
        return this;
    }

    public InventoryBuilder setSize(final int size) {
        this.invSize = size;
        return this;
    }

    public InventoryBuilder setItem(final int slot, final ItemStack item) {
        this.invItems.put(slot, item);
        return this;
    }

    public InventoryBuilder setHolder(final InventoryHolder holder) {
        this.invHolder = holder;
        return this;
    }

    public InventoryBuilder useFiller() {
        this.useFiller = true;
        return this;
    }

    public InventoryBuilder setFiller(final ItemStack filler) {
        this.fillerItem = filler;
        return this;
    }

    public Inventory build() {
        final Inventory inv = Bukkit.createInventory(this.invHolder, this.invSize, this.invName);
        for (final int slot : this.invItems.keySet()) {
            inv.setItem(slot, this.invItems.get(slot));
        }
        if (this.useFiller) {
            if (this.fillerItem == null) {
                throw new NullPointerException("Filler can not be null when using filler!");
            }
            for (int slot2 = 0; slot2 < this.invSize; ++slot2) {
                if (inv.getItem(slot2) == null) {
                    inv.setItem(slot2, this.fillerItem);
                }
            }
        }
        return inv;
    }
}
