package ch.verttigo.uhcmeetup.utils;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class Sounds
{
    private Player player;
    
    public Sounds(final Player p) {
        this.player = p;
    }
    
    public void playSound(final Sound s) {
        this.player.playSound(this.player.getLocation(), s, 8.0f, 8.0f);
    }
}
