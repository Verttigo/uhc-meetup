package ch.verttigo.uhcmeetup.utils;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.game.UHCState;
import ch.verttigo.uhcmeetup.task.EndTask;

public class WinChecker {
    public static boolean checkWin() {
        Boolean finish = false;
        if (UHCState.getState() == UHCState.INGAME && UHCMeetUp.getInstance().playerInGame.size() == 1) {
            finish = true;
            EndTask.startWinSequence();
        }
        return finish;
    }
}
