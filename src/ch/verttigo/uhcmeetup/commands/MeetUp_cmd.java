package ch.verttigo.uhcmeetup.commands;

import ch.verttigo.uhcmeetup.task.StartTask;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MeetUp_cmd implements CommandExecutor {
    public boolean onCommand(final CommandSender sender, final Command cmd, final String msg, final String[] args) {
        final Player p = (Player) sender;
        if (!cmd.getName().equalsIgnoreCase("meetup") || !p.hasPermission("meetup.admin")) {
            return false;
        }
        if (args.length == 0) {
            if (p.hasPermission("meetup.admin")) {
                p.sendMessage("§7§m-------------------------------");
                p.sendMessage("§6Game commands:");
                p.sendMessage("§» §e/meetup start §f- To start the game.");
                p.sendMessage("§» §e/meetup end §f- To stop the game.");
                p.sendMessage("§7 ");
                p.sendMessage("§6Scenario commands:");
                p.sendMessage("§» §e/scenario  §f- To show the commande for the scenario.");
            }
            p.sendMessage("§7§m-------------------------------");
            return false;
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("end") && p.hasPermission("meetup.admin")) {
                p.sendMessage("§cUHCMeetUp >> Force end...");
                Bukkit.broadcastMessage("§cGame ended by an administrator!");
                Bukkit.shutdown();
                return false;
            }
            if (args[0].equalsIgnoreCase("start") && p.hasPermission("meetup.admin")) {
                StartTask.start(p, true);
                return false;
            }
        }
        return false;
    }
}
