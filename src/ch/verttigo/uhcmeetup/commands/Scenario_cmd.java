package ch.verttigo.uhcmeetup.commands;

import ch.verttigo.uhcmeetup.scenario.ManageScenario;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class Scenario_cmd implements CommandExecutor, Listener {
    public boolean onCommand(final CommandSender sender, final Command cmd, final String msg, final String[] args) {
        final Player p = (Player) sender;
        if (cmd.getName().equalsIgnoreCase("scenario")) {
            if (args.length == 0) {
                p.sendMessage("§7§m-------------------------------");
                p.sendMessage("§» §e/scenario  §f- Display this");
                p.sendMessage("§» §e/scenario list §f- List all scenario.");
                p.sendMessage("§» §e/scenario add <Scenario Name> §f- Add a scenario with the Items in your inventory");
                p.sendMessage("§» §e/scenario del <Scenario Name> §f- del a scenario ");
                p.sendMessage("§» §e/scenario give <Scenario Name> §f- Give the inventory of the scenario");
                p.sendMessage("§» §e/scenario edit <Scenario Name> §f- Edit a scenario with the item in your inventory");
                p.sendMessage("§» §e/scenario setName <Scenario Name> <New Name> §f- Edit a scenario name");
                p.sendMessage("§» §e/scenario setLogo <Scenario Name>  §f- Set the scenario logo with the item in hand");
                p.sendMessage("§» §e/scenario lore list <Scenario Name> §f- Show lore with index");
                p.sendMessage("§» §e/scenario lore add <Scenario Name> <Lore> §f- add a lore for a scenario");
                p.sendMessage("§» §e/scenario lore del <Scenario Name> <index Lore> §f- add a lore for a scenario");
                p.sendMessage("§7§m-------------------------------");
                return false;
            }
            try {
                if (args.length >= 1) {
                    if (args[0].equalsIgnoreCase("list")) {
                        ManageScenario.sendScenario(p);
                    }
                    if (args[0].equalsIgnoreCase("add") && args[1] != null) {
                        ManageScenario.addScenario(p, args[1]);
                    }
                    if (args[0].equalsIgnoreCase("del") && args[1] != null) {
                        ManageScenario.removeScenario(p, args[1]);
                    }
                    if (args[0].equalsIgnoreCase("give") && args[1] != null) {
                        ManageScenario.giveScenarioInv(p, args[1]);
                    }
                    if (args[0].equalsIgnoreCase("edit") && args[1] != null) {
                        ManageScenario.editScenario(p, args[1]);
                    }
                    if (args[0].equalsIgnoreCase("setName") && args[1] != null && args[2] != null) {
                        ManageScenario.setName(p, args[1], args[2]);
                    }
                    if (args[0].equalsIgnoreCase("setLogo") && args[1] != null) {
                        ManageScenario.setItemLogo(p, args[1]);
                    }
                    if (args[0].equalsIgnoreCase("lore") && args[1].equalsIgnoreCase("list") && args[2] != null) {
                        System.out.println(args[2]);
                        ManageScenario.getLores(p, args[2]);
                    }
                    if (args[0].equalsIgnoreCase("lore") && args[1].equalsIgnoreCase("add") && args[2] != null && args[3] != null) {
                        ManageScenario.addLore(p, args[2], args[3]);
                    }
                    if (args[0].equalsIgnoreCase("lore") && args[1].equalsIgnoreCase("del") && args[2] != null && args[3] != null) {
                        try {
                            ManageScenario.delLores(p, args[2], Integer.parseInt(args[3]));
                        } catch (NumberFormatException e2) {
                            p.sendMessage("L'index entr\u00e9 n'est pas valide");
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
                p.sendMessage("Error dans la commande entr\u00e9e");
            }
        }
        return false;
    }
}
