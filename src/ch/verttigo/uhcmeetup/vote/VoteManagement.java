package ch.verttigo.uhcmeetup.vote;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.scenario.ManageScenario;
import ch.verttigo.uhcmeetup.scoreboard.ScoreBoardManager;
import ch.verttigo.uhcmeetup.utils.FileManager;
import ch.verttigo.uhcmeetup.utils.InventoryBuilder;
import ch.verttigo.uhcmeetup.utils.ItemBuilder;
import ch.verttigo.uhcmeetup.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class VoteManagement {
    public static HashMap<String, String> vote;
    public static String scenarioChoosed;

    static {
        VoteManagement.vote = new HashMap<String, String>();
        VoteManagement.scenarioChoosed = "default";
    }

    public static void setVotePlayer(final Player p, final String scenario) {
        if (VoteManagement.vote.containsKey(p.getDisplayName())) {
            VoteManagement.vote.replace(p.getDisplayName(), scenario);
        } else {
            VoteManagement.vote.put(p.getDisplayName(), scenario);
        }
        ScoreBoardManager.UHCScoreboard(p);
    }

    public static void pickedVote() {
        String picked = "";
        int count = 0;
        final ArrayList<String> picking = new ArrayList<String>();
        for (final String scenario : VoteManagement.vote.values()) {
            if (!picking.contains(scenario)) {
                picking.add(scenario);
            }
        }
        if (picking.isEmpty()) {
            picked = ManageScenario.listScenario().get(new Random().nextInt(ManageScenario.listScenario().size())).replace(".yml", "");
            count = 0;
        }
        for (final String scenario : picking) {
            final int accuracy = getFrequency(scenario);
            Bukkit.getPlayer("Verttigo").sendMessage(scenario + " : " + accuracy);
            if ((accuracy != 0 && count == accuracy) || scenario == "none") {
                picked = ManageScenario.listScenario().get(new Random().nextInt(ManageScenario.listScenario().size())).replace(".yml", "");
                Bukkit.broadcastMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-notunanime").replace("&", "§"));
                VoteManagement.scenarioChoosed = picked;
                break;
            }
            if (accuracy <= count) {
                continue;
            }
            picked = scenario;
            count = accuracy;
        }
        Bukkit.broadcastMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-picked").replace("&", "§").replace("%scenario%", new StringBuilder().append(picked).toString()));
    }

    public static int getFrequency(final String value) {
        int valueCount = 0;
        for (final String s : VoteManagement.vote.values()) {
            if (value.matches(s)) {
                ++valueCount;
            }
        }
        return valueCount;
    }

    public static String getVotePlayer(final Player p) {
        return VoteManagement.vote.get(p.getDisplayName());
    }

    public static void openVoteGUI(final Player p) {
        final int size = ManageScenario.listScenario().size();
        final InventoryBuilder voteGUI = new InventoryBuilder("Vote Management", Utils.getChestSize(size));
        for (int i = 0; i < size; ++i) {
            final String scenarioYML = ManageScenario.listScenario().get(i);
            final FileManager scenarioConfig = new FileManager(scenarioYML, "/Scenario");
            final String scenarioName = ManageScenario.listScenario().get(i).replace(".yml", "");
            final String itemLogo = scenarioConfig.getConfiguration().getString(scenarioName + ".ItemLogo");
            final ItemBuilder mat = new ItemBuilder();
            mat.setAmount(1);
            mat.setName(scenarioName);
            mat.setLore(scenarioConfig.getConfiguration().getStringList(scenarioName + ".lore"));
            mat.setMaterial(Material.getMaterial(itemLogo));
            voteGUI.useFiller();
            voteGUI.setFiller(new ItemStack(Material.STAINED_GLASS_PANE));
            voteGUI.setItem(i, mat.build());
        }
        p.openInventory(voteGUI.build());
    }
}
