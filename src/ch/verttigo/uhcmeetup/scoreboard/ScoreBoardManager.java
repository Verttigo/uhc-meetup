package ch.verttigo.uhcmeetup.scoreboard;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.game.UHCPvP;
import ch.verttigo.uhcmeetup.game.UHCState;
import ch.verttigo.uhcmeetup.task.StartTask;
import ch.verttigo.uhcmeetup.vote.VoteManagement;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class ScoreBoardManager {
    public static Map<Player, ScoreboardSign> waitingBoard;

    static {
        ScoreBoardManager.waitingBoard = new HashMap<Player, ScoreboardSign>();
    }

    public static void UHCScoreboard(final Player p) {
        final int players = UHCMeetUp.getInstance().playerInGame.size();
        if (!ScoreBoardManager.waitingBoard.containsKey(p)) {
            final ScoreboardSign scoreboard = new ScoreboardSign(p, UHCMeetUp.getInstance().getConfig().getString("Scoreboard.Waiting.name").replace("&", "§"));
            scoreboard.create();
            scoreboard.setLine(0, "§7§m--------------------");
            scoreboard.setLine(1, "§7");
            scoreboard.setLine(2, "§dVote:§f none");
            if (UHCMeetUp.getInstance().playerInGame.size() <= UHCMeetUp.getInstance().getConfig().getInt("min-player") && !StartTask.inStartMotion) {
                scoreboard.setLine(3, "§dWaiting for players");
            } else {
                scoreboard.setLine(3, "§dStart in:§f " + StartTask.timer);
            }
            scoreboard.setLine(4, "§dPlayers:§f " + players);
            scoreboard.setLine(5, "§a");
            scoreboard.setLine(6, "§7§m--------------------§a");
            scoreboard.setLine(7, UHCMeetUp.getInstance().getConfig().getString("Scoreboard.Waiting.custom-text").replace("&", "§"));
            ScoreBoardManager.waitingBoard.put(p, scoreboard);
        } else if (UHCState.getState() == UHCState.WAITING) {
            for (final Map.Entry<Player, ScoreboardSign> sign : ScoreBoardManager.waitingBoard.entrySet()) {
                sign.getValue().setLine(2, "§dVote:§f " + VoteManagement.getVotePlayer(sign.getKey()));
                if (UHCMeetUp.getInstance().playerInGame.size() <= UHCMeetUp.getInstance().getConfig().getInt("min-player") && !StartTask.inStartMotion) {
                    sign.getValue().setLine(3, "§dWaiting for players");
                } else {
                    sign.getValue().setLine(3, "§dStart in:§f " + StartTask.timer);
                }
                sign.getValue().setLine(4, "§dPlayers:§f " + players);
            }
        } else if (UHCState.getState() == UHCState.PREGAME || UHCState.getState() == UHCState.INGAME) {
            UHCMeetUp.getInstance();
            final int worldBorder = (int) UHCMeetUp.worldBorder.getSize() / 2;
            for (final Map.Entry<Player, ScoreboardSign> sign2 : ScoreBoardManager.waitingBoard.entrySet()) {
                sign2.getValue().setLine(2, "§dBorder:§f " + worldBorder + "§6;§f-" + worldBorder);
                if (UHCPvP.kills.get(sign2.getKey()) != null) {
                    sign2.getValue().setLine(3, "§dKills:§f " + UHCPvP.kills.get(sign2.getKey()));
                } else {
                    sign2.getValue().setLine(3, "§dKills:§f none");
                }
                sign2.getValue().setLine(4, "§dPlayers:§f " + players);
            }
        }
    }
}
