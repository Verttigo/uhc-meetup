package ch.verttigo.uhcmeetup.listener.game;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.game.UHCState;
import ch.verttigo.uhcmeetup.scoreboard.ScoreBoardManager;
import ch.verttigo.uhcmeetup.task.StartTask;
import ch.verttigo.uhcmeetup.utils.Utils;
import ch.verttigo.uhcmeetup.utils.WinChecker;
import ch.verttigo.uhcmeetup.vote.VoteManagement;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

public class UHCJoin implements Listener {
    private FileConfiguration config;
    private UHCMeetUp pl;

    public UHCJoin(final UHCMeetUp uhcmeetup) {
        this.pl = uhcmeetup;
        this.config = this.pl.getConfig();
    }

    @EventHandler
    public void onJoin(final PlayerJoinEvent e) {
        final Player p = e.getPlayer();
        if (!UHCState.isState(UHCState.WAITING)) {
            p.setGameMode(GameMode.SPECTATOR);
            ScoreBoardManager.UHCScoreboard(p);
            p.setAllowFlight(true);
        } else if (!UHCMeetUp.getInstance().playerInGame.contains(p.getUniqueId())) {
            if (StartTask.inStartMotion) {
                p.setLevel(StartTask.timer);
            } else {
                p.setLevel(0);
            }
            p.getInventory().clear();
            p.getInventory().setBoots(null);
            p.getInventory().setHelmet(null);
            p.getInventory().setLeggings(null);
            p.getInventory().setChestplate(null);
            p.setFoodLevel(666);
            p.setGameMode(GameMode.SURVIVAL);
            p.teleport(new Location(Bukkit.getWorld("world"), 0.0, 100.0, 0.0));
            VoteManagement.vote.put(p.getDisplayName(), "none");
            UHCMeetUp.getInstance().playerInGame.add(p.getUniqueId());
            for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
                final Player pl = Bukkit.getPlayer(uuid);
                ScoreBoardManager.UHCScoreboard(pl);
            }
            UHCMeetUp.getInstance().playerWhoPlayed = UHCMeetUp.getInstance().playerInGame;
            Utils.giveLobbyTools(p);
            e.setJoinMessage(this.config.getString("join-message").replace("&", "§").replace("%player%", new StringBuilder().append(e.getPlayer().getName()).toString()).replace("%online%", new StringBuilder().append(UHCMeetUp.getInstance().playerInGame.size()).toString()).replace("%max%", new StringBuilder().append(Bukkit.getMaxPlayers()).toString()));
            if (UHCMeetUp.getInstance().playerInGame.size() >= UHCMeetUp.getInstance().getConfig().getInt("min-player")) {
                StartTask.start(p, false);
            }
        }
    }

    @EventHandler
    public void onQuit(final PlayerQuitEvent e) {
        final Player p = e.getPlayer();
        if (UHCState.getState() == UHCState.WAITING) {
            e.setQuitMessage(this.config.getString("leave-message").replace("&", "§").replace("%player%", new StringBuilder().append(e.getPlayer().getName()).toString()).replace("%online%", new StringBuilder().append(UHCMeetUp.getInstance().playerInGame.size()).toString()).replace("%max%", new StringBuilder().append(Bukkit.getMaxPlayers()).toString()));
        }
        WinChecker.checkWin();
        UHCMeetUp.getInstance().playerInGame.remove(p.getUniqueId());
        UHCMeetUp.getInstance().playerWhoPlayed.remove(p.getUniqueId());
    }
}
