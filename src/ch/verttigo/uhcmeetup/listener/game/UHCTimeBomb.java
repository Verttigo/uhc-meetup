package ch.verttigo.uhcmeetup.listener.game;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class UHCTimeBomb implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    public void on(final PlayerDeathEvent event) {
        final Player player = event.getEntity();
        final Location loc = player.getLocation().clone();
        Block block = loc.getBlock();
        block = block.getRelative(BlockFace.DOWN);
        block.setType(Material.CHEST);
        final Chest chest = (Chest) block.getState();
        block = block.getRelative(BlockFace.NORTH);
        block.setType(Material.CHEST);
        for (final ItemStack item : event.getDrops()) {
            if (item != null) {
                if (item.getType() == Material.AIR) {
                    continue;
                }
                chest.getInventory().addItem(item);
            }
        }
        event.getDrops().clear();
        final ArmorStand stand = (ArmorStand) player.getWorld().spawn(chest.getLocation().clone().add(0.5, 1.0, 0.0), (Class) ArmorStand.class);
        stand.setVisible(true);
        stand.setGravity(false);
        stand.setCustomNameVisible(true);
        stand.setCustomNameVisible(true);
        stand.setSmall(true);
        stand.setGravity(false);
        stand.setVisible(false);
        stand.setMarker(true);
        new BukkitRunnable() {
            private int time = 16;

            public void run() {
                --this.time;
                if (this.time == 0) {
                    Bukkit.broadcastMessage("§a" + player.getName() + "'s §fcorpse has exploded!");
                    loc.getBlock().setType(Material.AIR);
                    loc.getWorld().createExplosion(loc.getBlockX() + 0.5, loc.getBlockY() + 0.5, loc.getBlockZ() + 0.5, 5.0f, false, true);
                    loc.getWorld().strikeLightning(loc);
                    stand.remove();
                    this.cancel();
                    return;
                }
                if (this.time == 1) {
                    stand.setCustomName("§4" + this.time + "s");
                } else if (this.time == 2) {
                    stand.setCustomName("§c" + this.time + "s");
                } else if (this.time == 3) {
                    stand.setCustomName("§6" + this.time + "s");
                } else if (this.time <= 15) {
                    stand.setCustomName("§e" + this.time + "s");
                } else {
                    stand.setCustomName("§a" + this.time + "s");
                }
            }
        }.runTaskTimer(UHCMeetUp.getInstance(), 0L, 20L);
    }
}
