package ch.verttigo.uhcmeetup.listener.game;

import ch.verttigo.uhcmeetup.game.UHCState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

public class UHCBuild implements Listener
{
    @EventHandler
    public void onPlace(final BlockPlaceEvent e) {
        if (UHCState.isState(UHCState.WAITING)) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onBreak(final BlockBreakEvent e) {
        if (UHCState.isState(UHCState.WAITING)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDrop(final PlayerDropItemEvent e) {
        if (UHCState.getState() == UHCState.WAITING || UHCState.getState() == UHCState.PREGAME) {
            e.setCancelled(true);
        } else {
            e.setCancelled(false);
        }
    }

    @EventHandler
    public void onClick(final InventoryClickEvent e) {
        if (!e.getWhoClicked().hasPermission("meetup.admin") && UHCState.isState(UHCState.WAITING)) {
            e.setCancelled(true);
        }
    }
}
