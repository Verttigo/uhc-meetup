package ch.verttigo.uhcmeetup.listener.game;

import ch.verttigo.uhcmeetup.game.UHCState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class UHCDamage implements Listener
{
    @EventHandler
    public void cancelDamage(final EntityDamageEvent e) {
        if (UHCState.isState(UHCState.WAITING) || UHCState.isState(UHCState.PREGAME) || UHCState.isState(UHCState.END)) {
            e.setCancelled(true);
        }
    }
}
