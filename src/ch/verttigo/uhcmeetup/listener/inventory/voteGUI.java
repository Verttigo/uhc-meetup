package ch.verttigo.uhcmeetup.listener.inventory;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.game.UHCState;
import ch.verttigo.uhcmeetup.vote.VoteManagement;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class voteGUI implements Listener {
    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        final ItemStack clicked = event.getCurrentItem();
        final Inventory inventory = event.getInventory();
        if (inventory.getName().equals("Vote Management") && UHCState.getState() == UHCState.WAITING) {
            event.setCancelled(true);
            if (clicked.getType() != Material.STAINED_GLASS_PANE) {
                player.sendMessage(UHCMeetUp.getInstance().getConfig().getString("vote").replace("&", "§").replace("%scenario%", new StringBuilder(clicked.getItemMeta().getDisplayName())));
                VoteManagement.setVotePlayer(player, clicked.getItemMeta().getDisplayName());
                player.closeInventory();
            }
        }
    }
}
