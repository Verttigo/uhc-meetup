package ch.verttigo.uhcmeetup.listener.inventory;

import ch.verttigo.uhcmeetup.game.UHCState;
import ch.verttigo.uhcmeetup.task.StartTask;
import ch.verttigo.uhcmeetup.vote.VoteManagement;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class playerInv implements Listener {
    @EventHandler
    public void onInventoryClick(final PlayerInteractEvent event) {
        final Player p = event.getPlayer();
        if ((event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) && UHCState.getState() == UHCState.WAITING) {
            event.setCancelled(true);
            if (p.getItemInHand().getType() == Material.CHEST) {
                VoteManagement.openVoteGUI(p);
                return;
            }
            if (p.getItemInHand().getType() == Material.COMMAND_MINECART) {
                StartTask.start(p, true);
            }
        }
    }
}
