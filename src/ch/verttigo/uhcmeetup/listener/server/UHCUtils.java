package ch.verttigo.uhcmeetup.listener.server;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class UHCUtils implements Listener {
    @EventHandler
    public void foodChange(final FoodLevelChangeEvent e) {
        e.setFoodLevel(666);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onWeatherChange(final WeatherChangeEvent event) {
        final boolean rain = event.toWeatherState();
        if (rain) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onThunderChange(final ThunderChangeEvent event) {
        final boolean storm = event.toThunderState();
        if (storm) {
            event.setCancelled(true);
        }
    }
}
