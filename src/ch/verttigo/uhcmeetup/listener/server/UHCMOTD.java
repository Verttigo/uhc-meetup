package ch.verttigo.uhcmeetup.listener.server;

import ch.verttigo.uhcmeetup.game.UHCState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class UHCMOTD implements Listener {
    @EventHandler
    public void onServerPing(final ServerListPingEvent e) {
        e.setMotd(UHCState.getState().name());
    }
}
