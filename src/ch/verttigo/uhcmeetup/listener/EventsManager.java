package ch.verttigo.uhcmeetup.listener;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.game.UHCPvP;
import ch.verttigo.uhcmeetup.listener.game.UHCBuild;
import ch.verttigo.uhcmeetup.listener.game.UHCDamage;
import ch.verttigo.uhcmeetup.listener.game.UHCJoin;
import ch.verttigo.uhcmeetup.listener.game.UHCTimeBomb;
import ch.verttigo.uhcmeetup.listener.inventory.playerInv;
import ch.verttigo.uhcmeetup.listener.inventory.voteGUI;
import ch.verttigo.uhcmeetup.listener.server.UHCMOTD;
import ch.verttigo.uhcmeetup.listener.server.UHCUtils;
import ch.verttigo.uhcmeetup.task.BorderTask;
import ch.verttigo.uhcmeetup.task.PreGameTask;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class EventsManager
{
    public static void registerEvents(final UHCMeetUp pl) {
        final PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new UHCJoin(pl), pl);
        pm.registerEvents(new PreGameTask(pl), pl);
        pm.registerEvents(new UHCPvP(pl), pl);
        pm.registerEvents(new BorderTask(pl), pl);
        pm.registerEvents(new UHCDamage(), pl);
        pm.registerEvents(new UHCBuild(), pl);
        pm.registerEvents(new UHCBuild(), pl);
        pm.registerEvents(new UHCUtils(), pl);
        pm.registerEvents(new UHCMOTD(), pl);
        pm.registerEvents(new voteGUI(), pl);
        pm.registerEvents(new playerInv(), pl);
        pm.registerEvents(new UHCTimeBomb(), pl);
    }
}
