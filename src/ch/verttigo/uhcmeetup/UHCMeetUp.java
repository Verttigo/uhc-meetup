package ch.verttigo.uhcmeetup;

import ch.verttigo.uhcmeetup.commands.MeetUp_cmd;
import ch.verttigo.uhcmeetup.commands.Scenario_cmd;
import ch.verttigo.uhcmeetup.game.UHCState;
import ch.verttigo.uhcmeetup.listener.EventsManager;
import ch.verttigo.uhcmeetup.utils.WorldGen;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.UUID;

public class UHCMeetUp extends JavaPlugin {
    public static UHCMeetUp instance;
    private FileConfiguration config;
    public ArrayList<UUID> playerInGame;
    public ArrayList<UUID> playerWhoPlayed;
    public static Economy economy;
    public static WorldBorder worldBorder;

    static {
        UHCMeetUp.worldBorder = null;
    }

    public static UHCMeetUp getInstance() {
        return UHCMeetUp.instance;
    }

    public UHCMeetUp() {
        this.playerInGame = new ArrayList<UUID>();
        this.playerWhoPlayed = new ArrayList<UUID>();
    }

    public static Economy getEconomy() {
        return UHCMeetUp.economy;
    }

    public void onEnable() {
        super.onEnable();
        UHCMeetUp.instance = this;
        this.config = this.getConfig();
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
        for (int i = 0; i < 5; ++i) {
            try {
                WorldGen.setup();
            } catch (Exception ex) {
            }
        }
        Bukkit.getServer().createWorld(new WorldCreator("UHC_World").environment(World.Environment.NORMAL).generateStructures(false).type(WorldType.NORMAL));
        Bukkit.getServer().getWorlds().add(Bukkit.getWorld("UHC_World"));
        Bukkit.getWorld("UHC_World").setGameRuleValue("naturalRegeneration", "false");
        Bukkit.getWorld("UHC_World").setDifficulty(Difficulty.PEACEFUL);
        EventsManager.registerEvents(this);
        this.getCommand("meetup").setExecutor(new MeetUp_cmd());
        this.getCommand("scenario").setExecutor(new Scenario_cmd());
        UHCState.setState(UHCState.WAITING);
        (UHCMeetUp.worldBorder = Bukkit.getWorld("UHC_World").getWorldBorder()).setCenter(0.0, 0.0);
        UHCMeetUp.worldBorder.setSize(400.0);
        if (!this.setupEconomy()) {
            System.out.println(String.format("[%s] - Disabled due to no Vault dependency found!", this.getDescription().getName()));
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        System.out.println("#UHC MeetUp> Enable#");
    }

    public void onDisable() {
        super.onDisable();
        System.out.println("#UHC MeetUp> Disabled#");
    }

    private boolean setupEconomy() {
        final RegisteredServiceProvider<Economy> economyProvider = (RegisteredServiceProvider<Economy>) this.getServer().getServicesManager().getRegistration((Class) Economy.class);
        if (economyProvider != null) {
            UHCMeetUp.economy = economyProvider.getProvider();
        }
        return UHCMeetUp.economy != null;
    }
}
