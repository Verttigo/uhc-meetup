package ch.verttigo.uhcmeetup.game;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.scoreboard.ScoreBoardManager;
import ch.verttigo.uhcmeetup.utils.WinChecker;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.HashMap;
import java.util.UUID;

public class UHCPvP implements Listener {
    public static HashMap<Player, Integer> kills;
    private FileConfiguration config;
    private UHCMeetUp pl;

    public UHCPvP(final UHCMeetUp uhcmeetup) {
        this.pl = uhcmeetup;
        this.config = this.pl.getConfig();
    }

    static {
        UHCPvP.kills = new HashMap<Player, Integer>();
    }

    private void addKill(final Player killer) {
        if (!UHCPvP.kills.containsKey(killer)) {
            UHCPvP.kills.put(killer, 0);
        }
        UHCPvP.kills.put(killer, UHCPvP.kills.get(killer) + 1);
    }

    @EventHandler
    public void onDeath(final PlayerDeathEvent e) {
        final Player victim = e.getEntity();
        final Player killer = e.getEntity().getKiller();
        e.setDeathMessage(null);
        if (e.getEntity() instanceof Player && UHCState.isState(UHCState.INGAME)) {
            if (killer instanceof Player) {
                ScoreBoardManager.UHCScoreboard(killer);
                this.addKill(killer);
                Bukkit.broadcastMessage(this.config.getString("death-message").replace("&", "§").replace("%killer%", new StringBuilder().append(killer.getName()).toString()).replace("%victim%", new StringBuilder().append(victim.getName()).toString()));
            } else {
                Bukkit.broadcastMessage(this.config.getString("death-unknown-message").replace("&", "§").replace("%victim%", new StringBuilder().append(victim.getName()).toString()));
            }
            UHCMeetUp.getInstance().playerInGame.remove(victim.getUniqueId());
            if (UHCMeetUp.getInstance().playerInGame.size() != 1) {
                Bukkit.broadcastMessage(this.config.getString("alive-players").replace("&", "§").replace("%players%", new StringBuilder().append(UHCMeetUp.getInstance().playerInGame.size()).toString()));
            }
            for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
                final Player pl = Bukkit.getPlayer(uuid);
                ScoreBoardManager.UHCScoreboard(pl);
            }
            victim.setGameMode(GameMode.SPECTATOR);
            WinChecker.checkWin();
        }
    }
}
