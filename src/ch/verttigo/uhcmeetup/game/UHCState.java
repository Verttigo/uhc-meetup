package ch.verttigo.uhcmeetup.game;

public enum UHCState {
    WAITING("WAITING", 0, true),
    PREGAME("PREGAME", 1, true),
    INGAME("INGAME", 2, false),
    END("END", 3, false);

    private boolean canJoin;
    private static UHCState currentState;

    UHCState(final String name, final int ordinal, final boolean canJoin) {
        this.canJoin = canJoin;
    }
    
    public boolean canJoin() {
        return this.canJoin;
    }
    
    public static void setState(final UHCState state) {
        UHCState.currentState = state;
    }
    
    public static boolean isState(final UHCState state) {
        return UHCState.currentState == state;
    }
    
    public static UHCState getState() {
        return UHCState.currentState;
    }
}
