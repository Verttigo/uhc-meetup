package ch.verttigo.uhcmeetup.scenario;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.utils.FileManager;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ManageScenario {
    public static void removeScenario(final Player p, final String scenario) {
        if (!listScenario().contains(scenario + ".yml")) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-notexist").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
            return;
        }
        final FileManager f = new FileManager(scenario + ".yml", "/Scenario");
        f.delete();
        p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-del").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
    }

    public static void addScenario(final Player p, final String scenario) {
        if (listScenario().contains(scenario + ".yml")) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-exist").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
            return;
        }
        final FileManager f = new FileManager(scenario + ".yml", "/Scenario");
        f.getConfiguration().addDefault(scenario + ".lore", null);
        f.getConfiguration().addDefault(scenario + ".ItemLogo", null);
        f.getConfiguration().set(scenario + ".name", scenario);
        f.getConfiguration().set(scenario + ".inv", p.getInventory().getContents());
        f.getConfiguration().set(scenario + ".armor", p.getInventory().getArmorContents());
        f.save();
        p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-add").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
    }

    public static void addLore(final Player p, final String scenario, final String lore) {
        if (!listScenario().contains(scenario + ".yml")) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-notexist").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
            return;
        }
        final FileManager f = new FileManager(scenario + ".yml", "/Scenario");
        try {
            final List loreList = f.getConfiguration().getList(scenario + ".lore");
            loreList.add(lore);
            f.getConfiguration().set(scenario + ".lore", loreList);
            f.save();
            p.sendMessage("lore add");
        } catch (NullPointerException e) {
            final List loreList2 = new ArrayList();
            loreList2.add(lore);
            f.getConfiguration().set(scenario + ".lore", loreList2);
            f.save();
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-lore-add").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
        }
    }

    public static void setItemLogo(final Player p, final String scenario) {
        if (!listScenario().contains(scenario + ".yml")) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-notexist").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
            return;
        }
        final FileManager f = new FileManager(scenario + ".yml", "/Scenario");
        f.getConfiguration().set(scenario + ".ItemLogo", p.getItemInHand().getType().toString());
        f.save();
        p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-logo").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
    }

    public static void setName(final Player p, final String scenario, final String newName) {
        if (!listScenario().contains(scenario + ".yml")) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-notexist").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
            return;
        }
        final FileManager f = new FileManager(scenario + ".yml", "/Scenario");
        f.getConfiguration().set(scenario + ".name", newName);
        f.save();
        p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-name").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
    }

    public static void getScenarioConfig(final Player p, final String scenario) {
    }

    public static void delLores(final Player p, final String scenario, final int index) {
        if (!listScenario().contains(scenario + ".yml")) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-notexist").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
            return;
        }
        final FileManager f = new FileManager(scenario + ".yml", "/Scenario");
        final List loreList = f.getConfiguration().getList(scenario + ".lore");
        if (loreList.get(index) != null) {
            loreList.remove(index);
            f.getConfiguration().set(scenario + ".lore", loreList);
            f.save();
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-lore-del").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
        } else {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-lore-index").replace("&", "§").replace("%index%", new StringBuilder().append(index)));
        }
    }

    public static void getLores(final Player p, final String scenario) {
        System.out.println(scenario);
        if (!listScenario().contains(scenario + ".yml")) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-notexist").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenario)));
            return;
        }
        final FileManager f = new FileManager(scenario + ".yml", "/Scenario");
        try {
            final List loreList = f.getConfiguration().getList(scenario + ".lore");
            for (int i = 0; i < loreList.size(); ++i) {
                p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-lore-item").replace("&", "§").replace("%index%", new StringBuilder().append(i)).replace("%lore%", new StringBuilder(loreList.get(i).toString())));
            }
        } catch (Exception e) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-lore-notexist").replace("&", "§"));
        }
    }

    public static void giveScenarioInv(final Player p, final String scenarioName) {
        if (!listScenario().contains(scenarioName + ".yml")) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-notexist").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenarioName)));
            return;
        }
        final FileManager scenario = new FileManager(scenarioName + ".yml", "/Scenario");
        p.getInventory().clear();
        ItemStack[] content = (ItemStack[]) ((List) scenario.getConfiguration().get(scenarioName + ".armor")).toArray(new ItemStack[0]);
        p.getInventory().setArmorContents(content);
        content = (ItemStack[]) ((List) scenario.getConfiguration().get(scenarioName + ".inv")).toArray(new ItemStack[0]);
        p.getInventory().setContents(content);
        p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-give").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenarioName)));
    }

    public static void editScenario(final Player p, final String scenarioName) {
        if (!listScenario().contains(scenarioName + ".yml")) {
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-notexist").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenarioName)));
            return;
        }
        final FileManager f = new FileManager(scenarioName + ".yml", "/Scenario");
        f.getConfiguration().set(scenarioName + ".inv", p.getInventory().getContents());
        f.getConfiguration().set(scenarioName + ".armor", p.getInventory().getArmorContents());
        f.save();
        p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-edit").replace("&", "§").replace("%scenario%", new StringBuilder().append(scenarioName)));
    }

    public static void sendScenario(final Player p) {
        final ArrayList<String> list = (ArrayList<String>) FileManager.getEveryConfig(new File(UHCMeetUp.getInstance().getDataFolder().getPath() + "/Scenario"));
        if (list.isEmpty()) {
            p.sendMessage("Il n'y a aucun scenario pour le moment");
            return;
        }
        p.sendMessage("§7§m-------------------------------");
        p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-list").replace("&", "§"));
        for (final String scenarioName : list) {
            final String scenarioWithoutYML = scenarioName.replace(".yml", "");
            p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("scenario-list-item").replace("&", "§").replace("%scenario%", new StringBuilder(scenarioWithoutYML)));
        }
        p.sendMessage("§7§m-------------------------------");
    }

    public static ArrayList<String> listScenario() {
        final ArrayList<String> list = (ArrayList<String>) FileManager.getEveryConfig(new File(UHCMeetUp.getInstance().getDataFolder().getPath() + "/Scenario"));
        return list;
    }
}
