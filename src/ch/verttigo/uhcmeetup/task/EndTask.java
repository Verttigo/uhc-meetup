package ch.verttigo.uhcmeetup.task;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.game.UHCState;
import ch.verttigo.uhcmeetup.utils.Sounds;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.UUID;

public class EndTask {
    static int task;
    public static int timer;

    static {
        EndTask.timer = 11;
    }

    public static void startWinSequence() {
        UHCState.setState(UHCState.END);
        Player winner = null;
        for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
            final Player pl = winner = Bukkit.getPlayer(uuid);
            pl.setGameMode(GameMode.SPECTATOR);
            new Sounds(pl).playSound(Sound.WITHER_DEATH);
            pl.sendMessage("§6§lYou win this game.");
            UHCMeetUp.getEconomy().depositPlayer(pl, UHCMeetUp.getInstance().getConfig().getInt("Rewards.Winner"));
        }
        for (final Player p : Bukkit.getOnlinePlayers()) {
            if (winner != p) {
                p.sendMessage("§6§l" + winner.getDisplayName() + " win this game.");
                UHCMeetUp.getEconomy().depositPlayer(p, UHCMeetUp.getInstance().getConfig().getInt("Rewards.Participation"));
            }
        }
        EndTask.task = Bukkit.getScheduler().scheduleSyncRepeatingTask(UHCMeetUp.getInstance(), () -> {
            --EndTask.timer;
            if (EndTask.timer == 5 || EndTask.timer == 4 || EndTask.timer == 3 || EndTask.timer == 2 || EndTask.timer == 1) {
                Bukkit.broadcastMessage(UHCMeetUp.getInstance().getConfig().getString("cooldown-end").replace("&", "§").replace("%time%", new StringBuilder().append(EndTask.timer).toString()));
            }
            if (EndTask.timer == 0) {
                Bukkit.shutdown();
            }
        }, 20L, 20L);
    }
}
