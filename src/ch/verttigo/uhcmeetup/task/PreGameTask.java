// 
// Decompiled by Procyon v0.5.36
// 

package ch.verttigo.uhcmeetup.task;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.game.UHCPvP;
import ch.verttigo.uhcmeetup.game.UHCState;
import ch.verttigo.uhcmeetup.utils.Sounds;
import ch.verttigo.uhcmeetup.utils.UHCTeleport;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.UUID;

public class PreGameTask implements Listener
{
    public static int timer;
    static int task;
    private static FileConfiguration config;
    private UHCMeetUp pl;

    public PreGameTask(final UHCMeetUp uhcmeetup) {
        this.pl = uhcmeetup;
        PreGameTask.config = this.pl.getConfig();
    }

    public static void startPreGame() {
        UHCState.setState(UHCState.PREGAME);
        Bukkit.getScheduler().cancelTask(StartTask.task);
        for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
            final Player pl = Bukkit.getPlayer(uuid);
            pl.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 11, 999));
            pl.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 11, 999));
            pl.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 11, 999));
            UHCTeleport.tpRandom(pl);
        }
        PreGameTask.task = Bukkit.getScheduler().scheduleSyncRepeatingTask(UHCMeetUp.getInstance(), new Runnable() {
            @Override
            public void run() {
                --PreGameTask.timer;
                if (PreGameTask.timer == 10 || PreGameTask.timer == 5 || PreGameTask.timer == 4 || PreGameTask.timer == 3 || PreGameTask.timer == 2 || PreGameTask.timer == 1) {
                    for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
                        final Player pl = Bukkit.getPlayer(uuid);
                        pl.sendMessage(PreGameTask.config.getString("pvp-message").replace("&", "§").replace("%time%", new StringBuilder().append(PreGameTask.timer).toString()));
                    }
                }
                if (PreGameTask.timer == 0) {
                    Bukkit.getScheduler().cancelTask(PreGameTask.task);
                    UHCState.setState(UHCState.INGAME);
                    for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
                        final Player pl = Bukkit.getPlayer(uuid);
                        new Sounds(pl).playSound(Sound.WITHER_SPAWN);
                        UHCPvP.kills.put(pl, 0);
                        pl.sendMessage(PreGameTask.config.getString("pvp-enable-message").replace("&", "§"));
                    }
                }
            }
        }, 20L, 20L);
    }

    static {
        PreGameTask.timer = 6;
    }
}
