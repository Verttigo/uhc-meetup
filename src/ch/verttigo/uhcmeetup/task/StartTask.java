package ch.verttigo.uhcmeetup.task;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.game.UHCState;
import ch.verttigo.uhcmeetup.scenario.ManageScenario;
import ch.verttigo.uhcmeetup.scoreboard.ScoreBoardManager;
import ch.verttigo.uhcmeetup.utils.Sounds;
import ch.verttigo.uhcmeetup.vote.VoteManagement;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.UUID;

public class StartTask {
    static int task;
    public static int timer;
    public static boolean inStartMotion;

    public static void setLevel(final int timer) {
        for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
            final Player pl = Bukkit.getPlayer(uuid);
            pl.setLevel(timer);
        }
    }

    static {
        StartTask.task = 0;
        StartTask.inStartMotion = false;
        StartTask.timer = 61;
    }

    public static void start(final Player p, final Boolean forceStart) {
        if ((UHCState.getState() == UHCState.WAITING && StartTask.task == 0) || forceStart) {
            if (forceStart && StartTask.timer > 11) {
                StartTask.timer = 11;
                p.sendMessage(UHCMeetUp.getInstance().getConfig().getString("force-start").replace("&", "§").replace("%player%", new StringBuilder(p.getName())));
            } else if (StartTask.inStartMotion && forceStart) {
                Bukkit.broadcastMessage(UHCMeetUp.getInstance().getConfig().getString("force-start-already").replace("&", "§"));
                return;
            }
            if (!StartTask.inStartMotion) {
                StartTask.task = Bukkit.getScheduler().scheduleSyncRepeatingTask(UHCMeetUp.getInstance(), () -> {
                    StartTask.inStartMotion = true;
                    setLevel(--StartTask.timer);

                    for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
                        final Player pl = Bukkit.getPlayer(uuid);
                        ScoreBoardManager.UHCScoreboard(pl);
                    }
                    if (UHCMeetUp.getInstance().playerInGame.size() == 0 || UHCMeetUp.getInstance().playerInGame.size() < UHCMeetUp.getInstance().getConfig().getInt("min-player")) {
                        Bukkit.getScheduler().cancelTask(StartTask.task);
                        Bukkit.broadcastMessage(UHCMeetUp.getInstance().getConfig().getString("player-notenough").replace("&", "§"));
                        StartTask.timer = 60;
                        StartTask.inStartMotion = false;

                        for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
                            final Player pl = Bukkit.getPlayer(uuid);
                            ScoreBoardManager.UHCScoreboard(pl);
                        }
                    } else {
                        if (StartTask.timer == 60 || StartTask.timer == 50 || StartTask.timer == 40 || StartTask.timer == 30 || StartTask.timer == 20 || StartTask.timer == 15 || StartTask.timer == 10 || StartTask.timer == 5 || StartTask.timer == 4 || StartTask.timer == 3 || StartTask.timer == 2 || StartTask.timer == 1) {

                            for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
                                final Player pl = Bukkit.getPlayer(uuid);
                                ScoreBoardManager.UHCScoreboard(pl);
                                pl.sendMessage(UHCMeetUp.getInstance().getConfig().getString("cooldown-start").replace("&", "§").replace("%time%", new StringBuilder().append(StartTask.timer).toString()));
                            }
                        }
                        if (StartTask.timer == 6) {
                            VoteManagement.pickedVote();
                        }
                        if (StartTask.timer == 0) {
                            Bukkit.getScheduler().cancelTask(StartTask.task);
                            BorderTask.BorderShrink();

                            for (final UUID uuid : UHCMeetUp.getInstance().playerInGame) {
                                final Player pl = Bukkit.getPlayer(uuid);
                                ScoreBoardManager.UHCScoreboard(pl);
                                pl.sendMessage(UHCMeetUp.getInstance().getConfig().getString("start-message").replace("&", "§"));
                                new Sounds(pl).playSound(Sound.ENDERDRAGON_DEATH);
                                ManageScenario.giveScenarioInv(pl, VoteManagement.scenarioChoosed);
                                pl.setGameMode(GameMode.SURVIVAL);
                            }
                            UHCMeetUp.getInstance().playerWhoPlayed = UHCMeetUp.getInstance().playerInGame;
                            PreGameTask.startPreGame();
                            ScoreBoardManager.UHCScoreboard(p);
                        }
                    }
                }, 20L, 20L);
            }
        }
    }
}
