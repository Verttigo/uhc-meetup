package ch.verttigo.uhcmeetup.task;

import ch.verttigo.uhcmeetup.UHCMeetUp;
import ch.verttigo.uhcmeetup.game.UHCState;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;

public class BorderTask implements Listener
{
    public static int timer;
    static int task;
    private static FileConfiguration config;
    private UHCMeetUp pl;

    public BorderTask(final UHCMeetUp uhcmeetup) {
        this.pl = uhcmeetup;
        BorderTask.config = this.pl.getConfig();
    }

    static {
        BorderTask.timer = 600;
    }

    public static void BorderShrink() {
        BorderTask.task = Bukkit.getScheduler().scheduleSyncRepeatingTask(UHCMeetUp.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (UHCState.getState() == UHCState.INGAME) {
                    --BorderTask.timer;
                    if (BorderTask.timer == 540) {
                        UHCMeetUp.getInstance();
                        UHCMeetUp.worldBorder.setSize(200.0, 60L);
                        Bukkit.broadcastMessage(BorderTask.config.getString("border-message").replace("&", "§").replace("%radius%", "100x100"));
                    }
                    if (BorderTask.timer == 360) {
                        UHCMeetUp.getInstance();
                        UHCMeetUp.worldBorder.setSize(100.0, 60L);
                        Bukkit.broadcastMessage(BorderTask.config.getString("border-message").replace("&", "§").replace("%radius%", "50x50"));
                    }
                    if (BorderTask.timer == 180) {
                        UHCMeetUp.getInstance();
                        UHCMeetUp.worldBorder.setSize(52.0, 60L);
                        Bukkit.broadcastMessage(BorderTask.config.getString("border-message").replace("&", "§").replace("%radius%", "25x25"));
                    }
                    if (BorderTask.timer == 0) {
                        Bukkit.getScheduler().cancelTask(BorderTask.task);
                        UHCMeetUp.getInstance();
                        UHCMeetUp.worldBorder.setSize(22.0, 60L);
                        Bukkit.broadcastMessage(BorderTask.config.getString("border-message").replace("&", "§").replace("%radius%", "10x10"));
                    }
                }
            }
        }, 20L, 20L);
    }
}
